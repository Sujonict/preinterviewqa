# Generated by Django 2.0 on 2018-09-16 06:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('preinterviewqaapp', '0013_preinterviewmodel_document'),
    ]

    operations = [
        migrations.AlterField(
            model_name='preinterviewmodel',
            name='document',
            field=models.FileField(upload_to='documents/'),
        ),
    ]
