from django.urls import path
from django.conf import settings
from django.contrib.staticfiles.views import serve
from django.conf.urls.static import static
from .views import SignUp, preinterview_question_answer_create_view, employee_list_view


app_name = 'preinterviewqaapp'

urlpatterns = [
    path('signup/', SignUp.as_view(), name='signup'),
    path('details/',employee_list_view, name='details'),
    path('', preinterview_question_answer_create_view, name='creates'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
